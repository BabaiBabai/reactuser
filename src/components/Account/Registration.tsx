import React,{ Component, FormEvent } from "react";
// import { OnChangeModel } from "../../common/types/Form.types";
// import { useDispatch } from "react-redux";
// import { login } from "../../store/actions/account.actions";
import TextInput from "../../common/components/TextInput";
import axios from 'axios'; 

// const Registration: React.FC = () => 
export default class Registration extends Component
{
  // const dispatch: Dispatch<any> = useDispatch();

  // const [formState, setFormState] = useState({
      
     
    // LocationId: { error: "", value: "" },
    // User_Password: { error: "", value: "" },
    // uid: { error: "", value: "" },
    // Name: { error: "", value: "" },
    // Designation: { error: "", value: "" },
    // Email: { error: "", value: "" },
    // Channel_Id: { error: "", value: "" },
    // Nonce: { error: "", value: "" } 
  // });
  state={
    LocationId:  "" ,
    User_Password: "" ,
    uid:  "" ,
    Name:  "" ,
    Designation: "" ,
    Email:  "" ,
    Profile_Image:"",
    Channel_Id:  "" ,
    Nonce:  "", 
    IsValidUser:false,   
    token:'',
    message:'',
    validation:{
        Email:'',
        User_Password:  '',
        Name:  '',
        Designation:''        
     }
 }
  constructor(props:any)
  {
  super(props); 
  this.submit=this.submit.bind(this);   
  this.isFormInvalid=this.isFormInvalid.bind(this);   
  this.getDisabledClass=this.getDisabledClass.bind(this);   
  this.formInputChange=this.formInputChange.bind(this);
  }
  
  isFormInvalid(event:any){
      let valid = true;
      this.setState({
        [event.target.name]:event.target.value
      });
      const validation:any=this.state.validation;
    
      if(!event.target.value)
      {      
        valid = false;        
        validation[event.target.name]='Can not be empty';
      }
      else
      {
        validation[event.target.name]='';
      }
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

      if (event.target.name==='Email'&& (!pattern.test(event.target.value))) 
      {
        valid = false;   
          validation.Email = "Please enter valid email address.";
          //return false;
      } 
      else
      {
          validation.Email = "";
      }
      this.setState({
        validation
      });
      return valid ;

  }
  formInputChange(event:any){
    let valid = true;
    this.setState({
       [event.field]:event.value
    });
    const validation:any=this.state.validation;
   
    if(!event.value)
    {   
        valid = false;          
       validation[event.field]='Can not be empty';
    }
    else
    {
       validation[event.field]='';
    }
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  
    if (event.field==='Email'&& (!pattern.test(event.value))) 
    {
      valid = false;  
      validation[event.field] = "Please enter valid email address.";
        //return false;
    } 
    else
    {
      validation[event.field] = null;
    }
    this.setState({
       validation
    });
    return valid;
  }
  submit(event:any){
    event.preventDefault();
   
    const validation:any=this.state.validation;
   
   //#region input validation
    // if(!event.target.title.value)
    // {           
    //    validation[event.target.title.name]='Can not be empty';
    // }     
    // if(!event.target.body.value)
    // {           
    //    validation[event.target.body.name]='Can not be empty';
    // }
    // if(color===null)
    // {           
    //    validation["noteColor"]='Can not be empty';
    // }     
     
    // //#endregion
    // event._targetInst.stateNode.map((item:any)=> {
    //   if(event._targetInst.stateNode[item].value=="")
    //   {
    //     let controlId=event._targetInst.stateNode[0].id;
    //     const idArray:any[]=controlId.split('id_');
    //     controlId=idArray[2];
    //     validation[controlId]='Can not be empty';
    //   }
    // });
    
      if(validation.Name==='Can not be empty'||validation.Name==="")
      {
          validation.Name='Can not be empty';
          this.setState({
            validation
            });
             return false;
      }
      else if(validation.Designation==='Can not be empty'||validation.Designation==="")
      {
          validation.Designation='Can not be empty';
          this.setState({
          validation
          });
           return false;
      }
      else if(validation.Email==='Can not be empty'||validation.Email==="")
      {
          validation.Email='Can not be empty';
          this.setState({
            validation
            });
            return false;
      }
      else if(validation.Email==="Please enter valid email address.")
      {
          validation.Email='Please enter valid email address.';
          this.setState({
            validation
            });
            return false;
      }
      else if(
      validation.User_Password==='Can not be empty'||validation.User_Password==="")
      {
          validation.User_Password='Can not be empty';
          this.setState({
            validation
            });
            return false;
      }
   
    else
    {
        console.log("Validation Success");
       axios.post('http://localhost:10024/api/AppUser/SaveAppUserDetails', {
        'User_Password':this.state.User_Password,
        'uid':'',
        'Name':this.state.Name,
        'Designation':this.state.Designation,
        'Email':this.state.Email,
        "Profile_Image":'',
        'Channel_Id':'',
        'Nonce':'',
        'LocationId':1
        } ).then(res=>{
        console.log(res.data);
        let responseData=res.data;
        if(!responseData.status && responseData.Message==="User already exist")
        {
         alert("User already exist");
        }
        else if(responseData.status && responseData.Message==="User Created Successfully")
        {
          alert("User Created Successfully");
          // window.location.reload(false);
          window.location.href = "/login";
        }
        else  
        {
          alert("Some error Occured!!");
        }     
        
        }).catch(error => {
            console.log(error);
            alert("Some error Occured");
            // this.setState({
            //     message:"Server error.."
            // });
          });
    }

    // dispatch(login(formState.email.value));
    }

  getDisabledClass(event:any){
    let isError: boolean = this.isFormInvalid(event) as boolean;
    return isError ? "disabled" : "";
  }
  

  render(){
  return (

    <div className="container">
      <div className="row justify-content-center">
        <div className="col-xl-10 col-lg-12 col-md-9">
          <div className="card o-hidden border-0 shadow-lg my-5">
            <div className="card-body p-0">
              <div className="row">
                <div className="col-lg-6 d-none d-lg-block bg-login-image"></div>
                <div className="col-lg-6">
                  <div className="p-5">
                    <div className="text-center">
                      <h1 className="h4 text-gray-900 mb-4">Welcome!</h1>
                    </div>
                    <form className="user" onSubmit={this.submit}>
                     
                      <div className="form-group">
                      <TextInput id="Name"
                          field="Name"
                          value={this.state.Name}
                          onChange={this.formInputChange}
                          required={true}
                          maxLength={100}
                          label="Name"
                          placeholder="Name" />
                          <span className="error" style={{ color: 'red' }}>
                            {this.state.validation.Name?this.state.validation.Name:null}
                        </span>
                          </div>
                      <div className="form-group">
                      <TextInput id="Designation"
                          field="Designation"
                          value={this.state.Designation}
                          onChange={this.formInputChange}
                          required={true}
                          maxLength={100}
                          label="Designation"
                          placeholder="Designation" />
                        <span className="error" style={{ color: 'red' }}>
                            {this.state.validation.Designation?this.state.validation.Designation:null}
                        </span>
                      </div>
                       
                      
                      <div className="form-group">
                        <TextInput id="Email"
                          field="Email"
                          value={this.state.Email}
                          onChange={this.formInputChange}
                          required={true}
                          maxLength={100}
                          label="Email"
                          placeholder="Email" />
                           <span className="error" style={{ color: 'red' }}>
                            {this.state.validation.Email?this.state.validation.Email:null}
                        </span>
                      </div>
                      <div className="form-group">
                        <TextInput id="User_Password"
                          field="User_Password"
                          value={this.state.User_Password}
                          onChange={this.formInputChange}
                          required={true}
                          maxLength={100}
                          type="password"
                          label="Password"
                          placeholder="Password" />
                            <span className="error" style={{ color: 'red' }}>
                            {this.state.validation.User_Password?this.state.validation.User_Password:null}
                        </span>
                      </div>                      
                      <button
                        className={`btn btn-primary btn-user btn-block ${this.getDisabledClass}`}
                        type="submit">
                        Sign Up
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  }
}

// export default Registration;
