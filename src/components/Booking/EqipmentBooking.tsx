import React from "react"; 
import axios from 'axios'; 
import ReactModal from 'react-modal'; 
import "../Master/css/Project.css"; 
import EquipmentListing from "./EquipmentListing";
import CalenderNew from '../Booking/calenderNew';
// export default class ProjectForm extends Component {
    export interface IEqipmentbookingProp
    {
      // :{_id:string,Equipment_Name: '',Location_Id:string,Min_Batch_Size:string,Max_Batch_Size:string,SetUp_Time:string,
      // Process_Time:string,Cleaning_Time:string,Status_Id:string,Category_Id:boolean,validation:any,LocationList:any,
      // SelectedEquipmentList:any[],formEuipmentInputChange:any,saveEquipmentBooking:any,formEuipmentLocationChange:any,getSelectedEqipmentList:any}=
        Eqipmentbooking:{
                        _id:'',
                        Equipment_Name: '',
                        Location_Id: '',
                        Min_Batch_Size:'',
                        Max_Batch_Size:'', 
                        SetUp_Time:'',
                        Process_Time:'', 
                        Cleaning_Time:'',
                        Status_Id:'', 
                        Category_Id:'', 
                        validation: {
                                    Equipment_Name: null,
                                    Location_Id: null,
                                    Min_Batch_Size:null,
                                    Max_Batch_Size:null  ,
                                    SetUp_Time:null  ,
                                    Process_Time:null  ,
                                    Cleaning_Time:null  ,
                                    Status_Id:null  ,
                                    Category_Id:null  ,
                                    IsEuipmentSelected:null
                                     }, 
                        LocationList:[{
                                    id: '',
                                    Desc: '' 
                                    }],   
                        EqipmentListData:[{
                                    id: '',
                                    Desc: '' 
                                    }],
                      SelectedEquipmentList:[],            
                      showModal: false,
                      showModelEquipmentId:'',
                      showCalenderModel:false
          },
        
    formEuipmentInputChange(event:any):any,
    saveEquipmentBooking(event:any):any,
    formEuipmentLocationChange(event:any):any
    getSelectedEqipmentList(event:any):any,
    
    deleteEquipment(id:string):any,
    OpenEquipmentSelectedModal(id:string):any ,
    handleEquipmentCloseModal(event:any):any ,
    onSelectCalenderBooking(id:string):any 
    }   
const EqipmentBooking=(props:IEqipmentbookingProp)=>{
     return (      
      
            <div className="col-xl-7 col-lg-7">
            <div className="card shadow mb-4">
              <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-green">Equipment</h6>
              </div>
              <div className="card-body"> 
            <form onSubmit={()=>{}}>
                {/* //props.formSubmit */}
               
                 <div className="form-group">
                         <label>Location</label>
                         <select name='Location_Id' className="form-control error" onChange={props.formEuipmentLocationChange}  >
                            <option  key={'Select'} value={'Select'} > Select </option>
                                {props.Eqipmentbooking.LocationList.map(item => (
                                   
                                   <option key={item.id} value={item.id} > {item.Desc} </option>
                                       ))}
                          </select>
                          {props.Eqipmentbooking.validation.Location_Id ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Location_Id}</span> : null}

                  </div>
                  <div className="form-group">
                     <label>Eqipment</label>
                     {/* <input type="text" name="Equipment_Name" placeholder="Equipment_Name" className="form-control" value={props.Eqipmentbooking.Equipment_Name} onChange={props.formEuipmentInputChange} /> */}
                     <select name='Location_Id' className="form-control error" onChange={props.getSelectedEqipmentList}  >
                            <option  key={'Select'} value={'Select'} > Select </option>
                                {props.Eqipmentbooking.EqipmentListData.map(item => (
                                   <option  key={item.id} value={item.id} > {item.Desc} </option>
                                       ))}
                          </select>
                     {props.Eqipmentbooking.validation.IsEuipmentSelected ? <span className="error title-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.IsEuipmentSelected}</span> : null}
                 </div>
                 <div className="form-group">
                 {props.Eqipmentbooking.SelectedEquipmentList.map((item: any)=>(
                       <div className="col col-12 card" key={item._id}>
                        <EquipmentListing data={item}
                         OnDelete={(id)=>props.deleteEquipment(id)} 
                         OpenEquipmentSelectedModal={(id)=>{props.OpenEquipmentSelectedModal(id)}} 
                         showModal={props.Eqipmentbooking.showModal}
                         showModelEquipmentId={props.Eqipmentbooking.showModelEquipmentId}
                         showCalenderModel={props.Eqipmentbooking.showCalenderModel}
                         onSelectCalenderBooking={(id)=>{props.onSelectCalenderBooking(id)}}
                         ></EquipmentListing>
                        </div>
                      ))}
                </div>
                <ReactModal className="projectListModel" isOpen={props.Eqipmentbooking.showModal} >
                <div>
                    <div className="row col col-12 content user mb-4 ml-4 mr-4">                 
                    <button onClick={props.handleEquipmentCloseModal}>Close Modal</button>               
                    </div>                
                    {/* <div className="row col col-12 text-center " > */}
                      
                      {  props.Eqipmentbooking.SelectedEquipmentList.map((lisdata:any) =>{    
                              if(lisdata._id===props.Eqipmentbooking.showModelEquipmentId) 
                              {
                              return ( 
                                <div className="row col col-12 content user mb-4 ml-4 mr-4 " >                           
                                      <div className="row col col-12 card"  style={{margin: 0}}>
                    
                                          <div className="col-12">
                                          <label><b>Equipment_Name</b></label>
                                          </div>
                                          <div className="col-12">
                                             {lisdata.Equipment_Name}
                                           </div>                        
                                      </div>
                                      <div className="row col col-12 card"  style={{margin: 0}}>
                    
                                          <div className="col-12">
                                          <label><b>Equipment Id</b></label>
                                          </div>
                                          <div className="col-12">
                                          {lisdata._id}
                                          </div>                        
                                      </div>
                                    
                                    
                                     
                                      
                                        {/* <div className="form-group">
                                        <label>Min Batch Size</label>
                                            {lisdata.Min_Batch_Size}
                                        </div>
                                        <div className="form-group">
                                        <label>Max Batch Size</label>
                                            {lisdata.Max_Batch_Size}
                                        </div>
                                        <div className="form-group">
                                        <label>SetUp Time</label>
                                            {lisdata.SetUp_Time}
                                        </div> */}
                                        </div> 
                                         
  
           )}})} 
                    {/* </div>                   */}
                    </div>
                </ReactModal>

                <ReactModal className="projectListModel" isOpen={props.Eqipmentbooking.showCalenderModel} >
                <div>
                        <div className="row col col-12 content user mb-4 ml-4 mr-4">                 
                        <button onClick={props.handleEquipmentCloseModal}>Close Modal</button>               
                      sdfsdgdfgdfgfg
                        </div>               

                        <div className="row col col-12 content user mb-4 ml-4 mr-4 " >                           
                            <div className="row col col-12 card"  style={{margin: 0}}>
                            <CalenderNew></CalenderNew>
                            </div>                        
                        </div>
                 </div>
                </ReactModal>
              <div className="form-group text-center">
                <button type="submit" className='btn btn-info' style={{marginTop:'10px'}} onClick={props.saveEquipmentBooking}>Save</button>               
              </div>              
            </form>
            </div>
            </div>
          </div> 
       );
 }

 export default EqipmentBooking;
 


//  <div className="form-group">
//  <label>Equipment_Name</label>
//  <input type="text" name="Equipment_Name" placeholder="Equipment_Name" className="form-control" value={props.Eqipmentbooking.Equipment_Name} onChange={props.formEuipmentInputChange} />
//  {props.Eqipmentbooking.validation.Equipment_Name ? <span className="error title-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Equipment_Name}</span> : null}
// </div>
// <div className="form-group">
// <label>Location</label>
// <select name='Location_Id' className="form-control error" onChange={props.formEuipmentInputChange}  >
// <option  key={'Select'} value={'Select'} > Select </option>
//        {props.Eqipmentbooking.LocationList.map(item => (
//         <option  key={item.id} value={item.id} > {item.Desc} </option>
//        ))}
//      </select>
//      {props.Eqipmentbooking.validation.Location_Id ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Location_Id}</span> : null}

// </div>
// <div className="form-group">
// <label>Min_Batch_Size</label>
//  <input type="number" name="Min_Batch_Size" placeholder="Min_Batch_Size" className="form-control" value={props.Eqipmentbooking.Min_Batch_Size} onChange={props.formEuipmentInputChange} />
//  {props.Eqipmentbooking.validation.Min_Batch_Size ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Min_Batch_Size}</span> : null}
// </div>
// <div className="form-group">
// <label>Max_Batch_Size</label>
//  <input type="number" name="Max_Batch_Size" placeholder="Max_Batch_Size" className="form-control" value={props.Eqipmentbooking.Max_Batch_Size} onChange={props.formEuipmentInputChange} />
//  {props.Eqipmentbooking.validation.Max_Batch_Size ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Max_Batch_Size}</span> : null}
// </div>
// <div className="form-group">
// <label>SetUp_Time</label>
//  <input type="number" name="SetUp_Time" placeholder="SetUp_Time" className="form-control" value={props.Eqipmentbooking.SetUp_Time} onChange={props.formEuipmentInputChange} />
//  {props.Eqipmentbooking.validation.SetUp_Time ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.SetUp_Time}</span> : null}
// </div>

// <div className="form-group">
// <label>Process_Time</label>
//  <input type="number" name="Process_Time" placeholder="Process_Time" className="form-control" value={props.Eqipmentbooking.Process_Time} onChange={props.formEuipmentInputChange} />
//  {props.Eqipmentbooking.validation.Process_Time ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Process_Time}</span> : null}
// </div>
// <div className="form-group">
// <label>Cleaning_Time</label>
//  <input type="number" name="Cleaning_Time" placeholder="Cleaning_Time" className="form-control" value={props.Eqipmentbooking.Cleaning_Time} onChange={props.formEuipmentInputChange} />
//  {props.Eqipmentbooking.validation.Cleaning_Time ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Cleaning_Time}</span> : null}
// </div>
// <div className="form-group">
// <label>Status_Id</label>
//  <input type="text" name="Status_Id" placeholder="Status_Id" className="form-control" value={props.Eqipmentbooking.Status_Id} onChange={props.formEuipmentInputChange} />
//  {props.Eqipmentbooking.validation.Status_Id ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Status_Id}</span> : null}
// </div>
// <div className="form-group">
// <label>Category_Id</label>
//  <input type="text" name="Category_Id" placeholder="Category_Id" className="form-control" value={props.Eqipmentbooking.Category_Id} onChange={props.formEuipmentInputChange} />
//  {props.Eqipmentbooking.validation.Category_Id ? <span className="error body-error" style={{ color: 'red' }}>{props.Eqipmentbooking.validation.Category_Id}</span> : null}
// </div>        

 
  