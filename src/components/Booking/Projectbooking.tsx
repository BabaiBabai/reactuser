import React, { Component } from "react"; 
import axios from 'axios'; 
 
import "../Master/css/Project.css";
import { Link } from "react-router-dom";
import ProjectList from "../Master/MasterDataList/ProjectList";
import Collapsible from "react-collapsible";
 
// export default class ProjectForm extends Component {
    export interface IProjectbookingProp
    {
        Projectbooking :{
            projectNo: '',
            projectName: '',
            projectLead:'',
            IsDelete:false, 
            validation: {
                projectNo: null,
                projectName: null,
                projectLead:null
                //,
               // IsDelete:null
            },    
            isUpdate:false,
            buttonName:'Save',
            projectListData:[]
           
          }
         ,    
         handleOpenModal(event:any):any ,
    //     OnformInputChange(event:any):any,
      
    //    formColorChange(color:any):any,
    //    UpdateNote(event:any):any
    formProjectInputChange(event:any):any 
    ,
 
    saveProjectBooking(event:any):any
    }   
const Projectbooking=(props:IProjectbookingProp)=>{
     return (
        
         <div className="col-xl-7 col-lg-7">         
         <div className="card mb-4">
           <div className="card-header py-3">
             <h6 className="m-0 font-weight-bold text-green">Enter Project Details</h6>
            
             <a style={{cursor:'pointer',color: 'blue',float: 'right'}} onClick={(event)=>{props.handleOpenModal(event)}}>Explore Project list </a>
           </div>
           <div className="card-body"> 
         <form onSubmit={()=>{}}>
           <div className="form-group">
             <label>Project Number</label>
             <input type="text" name="projectNo" placeholder="Project Number..." className="form-control" 
             value={props.Projectbooking.projectNo} onChange={(event)=>{props.formProjectInputChange(event)}} />  
             {props.Projectbooking.validation.projectNo ? <span className="error title-error" style={{ color: 'red' }}>{props.Projectbooking.validation.projectNo}</span> : null}
           </div>
           <div className="form-group">
           <label>Project Name</label>
             <input type="text" name="projectName" placeholder="Project Name..." className="form-control"  value={props.Projectbooking.projectName} onChange={(event)=>{props.formProjectInputChange(event)}} />
             {props.Projectbooking.validation.projectName ? <span className="error body-error" style={{ color: 'red' }}>{props.Projectbooking.validation.projectName}</span> : null}
           </div>
           <div className="form-group">
           <label>Project Lead</label>
             <input type="text" name="projectLead" placeholder="Project Lead..." className="form-control"  value={props.Projectbooking.projectLead} onChange={(event)=>{props.formProjectInputChange(event)}} />
             {props.Projectbooking.validation.projectLead ? <span className="error body-error" style={{ color: 'red' }}>{props.Projectbooking.validation.projectLead}</span> : null}
           </div>   
           <div className="form-group text-center">
               <button type="submit" className='btn btn-info' style={{marginTop:'10px'}} onClick={props.saveProjectBooking}>Save</button>   
           </div>
         </form>
         </div>
         </div>
       </div> 
     );
 }

 export default Projectbooking;
//   constructor(props: any) {
//     super(props);
//     this.formInputChange = this.formInputChange.bind(this);
//     this.formSubmit = this.formSubmit.bind(this);
//   }
//   componentDidMount(){
//     axios.get('http://localhost:10024/api/project/getAllProject').then(res=>{
//          console.log(res.data);
//          if(res.data!==null && res.data.IsSuccess)
//          {
//             this.setState({
//                 projectListData:res.data.ResponseResult
//             });
//          }             
        
//         }).catch(error => {
//             console.log(error);
            
//             // this.setState({
//             //     message:"Server error.."
//             // });
//           });

// }
 

//   formInputChange(event: any) { 

//     this.setState({
//       [event.target.name]: event.target.value
//     });

//     const validation: any = props.Projectbooking.validation;
//     if (!event.target.value) {
//       validation[event.target.name] = 'Can\'t be empty';
//     } else {
//       validation[event.target.name] = '';
//     }
//     this.setState({
//       validation
//     });
//   }
 
//   UpdateNote(){
    
//   }
// CheckAllValidation(event:any){
//   let valid = true;
//   const validation: any = props.Projectbooking.validation;
   
//     if(!event.target.projectNo.value)
//     {   
//        valid = false;        
//        validation[event.target.projectNo.name]='Can not be empty';
//     }     
//     if(!event.target.projectName.value)
//     {  
//         valid = false;         
//        validation[event.target.projectName.name]='Can not be empty';
//     }
//     if(!event.target.projectLead.value)
//     {    
//         valid = false;       
//        validation[event.target.projectLead.name]='Can not be empty';
//     }
// this.setState({
//   validation
// });
//  return valid
// }

//   formSubmit(event: any) {
//     event.preventDefault();

//     if(this.CheckAllValidation(event))
//     {
  
//     // if(!props.Projectbooking.NoteId){
         
//         axios.post('http://localhost:10024/api/project/createProject', {
//             'ProjectNo':event.target.projectNo.value,
//             'ProjectName':event.target.projectName.value,
//             'ProjectLead':event.target.projectLead.value,
//             'IsDelete':false,
//           } ).then(res=>{
//              console.log(res.data);
//              let responseData=res.data;
//              if(!responseData.status && responseData.Message==="Project already exist")
//              {
//               alert("Project already exist");
//              }
//             else if(responseData.status && responseData.Message==="Project Created Successfully")
//             {
//               alert("Project Created Successfully");
//               window.location.reload(false);
//             }
//             else
//             {
//               alert("Some error Occured!!");
//             }     
            
//             }).catch(error => {
//                 console.log(error);
//                 alert("Some error Occured");
//                 // this.setState({
//                 //     message:"Server error.."
//                 // });
//               });
//     // }
//     // else{
//     //   this.UpdateNote()
//     // }
//     this.setState({ 
//       title: '',
//       body: '',
//       noteColor:'',
//       creator:''
//     });
//   }
//   }
 
  