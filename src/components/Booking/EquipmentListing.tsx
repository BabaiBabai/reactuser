
import React from'react';
import MyCalendar from './calender'
import { Calendar } from 'react-big-calendar';
import CalenderNew from '../Booking/calenderNew';
import ReactModal from 'react-modal'; 
import "../Master/css/Project.css"; 
export interface IEquipmentbookingList
{
    data :{
        _id:''
        Equipment_Name: '',
        Location_Id: '',
        Min_Batch_Size:'',
        Max_Batch_Size:'', 
        SetUp_Time:'',
        Process_Time:'', 
        Cleaning_Time:'',
        Status_Id:'', 
        Category_Id:'',
    } , 
    showModal: false, 
    showModelEquipmentId:'', 
    showCalenderModel:false , 
    OnDelete(id:string):any,
    OpenEquipmentSelectedModal(id:string):any ,
    onSelectCalenderBooking(id:string):any
}   
const EquipmentListing=(props:IEquipmentbookingList)=>{
    return (
        
        <div className="col-xl-12 col-lg-12">  
         <div className="col-12"> 
                    <div className="row"  style={{margin: 0}}>
                    
                        <div className="col-8">
                          <label><b>Equipment Id</b></label>
                        </div>
                        <div className="col-2">
                        <button className="btn btn-info ml-2" onClick={()=>props.OnDelete(props.data._id)}>X</button>
                        </div>
                        <div className="col-12">
                          {props.data._id}
                          </div>                        
                    </div>
                    <div className="row" style={{margin: 0}}>
                   
                        <div className="col-7">
                        {/* <label>Project Name</label> */}
                        <a style={{textDecoration:'underline'}} onClick={()=>{props.OpenEquipmentSelectedModal(props.data._id)}}>View additional Details</a>
                        </div>
                        <div className="col-3">
                       {/* calender */}
                       {/* <CalenderNew></CalenderNew> */}
                          </div>                        
                    </div>
                    
                    <div className="row" style={{margin: 0}}>
                   
                     
                        <div className="col-12">
                    {/* <button onClick={()=>{}}>Select</button> */}
                    <a style={{textDecoration:'underline'}} onClick={()=>{props.onSelectCalenderBooking(props.data._id)}}>View additional Details</a>
                       {/* calender */}
                        {/* <CalenderNew></CalenderNew> */}
                          </div>                        
                    </div>
                    
                    {/* <ReactModal className="projectListModel" isOpen={props.showCalenderModel} >
                        <div>
                       
                         zsdfsdf
                        </div>
                    </ReactModal> */}


                      </div>
                    <div className="col-4">
                        {/* <button onClick={props.onSelectProject(props)}>Select</button> */}
                        {/* <button className="btn btn-info mt-4" onClick={()=>props.onSelectProject(props)}>Select</button> */}
                     </div>
                    <br/>
        </div> 
    );
}
 
export default EquipmentListing;