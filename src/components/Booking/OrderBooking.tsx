import React,{ Component } from "react";
import axios from 'axios'; 
import 'react-tabs/style/react-tabs.css';
import Projectbooking from "./Projectbooking";
import ReactModal from 'react-modal'; 
import ProjectList from "../Master/MasterDataList/ProjectList";
import './css/OrderBooking.css'
import EqipmentBooking from "./EqipmentBooking";
import EquipmentListing from "./EquipmentListing";
import Reviewbooking from "./ReviewBoking";
import { v4 as uuidv4 } from 'uuid';
import BookingStatus from "./BookingStatus";
 
export default class OrderBooking extends Component
{
   
  state:{LocationId:string,User_Password:string,uid:string,Name:string,Designation:string,Email:string,Profile_Image:string,
         Channel_Id:string,Nonce:string,IsValidUser:boolean,token:string,message:string,validation:any,Projectbooking:any,
         tabIndex:number,isProjectBooking:boolean,isEquipmentBooking:boolean,isReviewBooking:boolean,showModal:boolean,
         projectListData:any[],SelectedProjectId:string,Eqipmentbooking:any,SelectedLocationId:string,BookingStatus:any}={
        LocationId:  "" ,
        User_Password: "" ,
        uid:  "" ,
        Name:  "" ,
        Designation: "" ,
        Email:  "" ,
        Profile_Image:"",
        Channel_Id:  "" ,
        Nonce:  "", 
        IsValidUser:false,   
        token:'',
        message:'',
        validation:{
                  Email:'',
                  User_Password:  '',
                  Name:  '',
                  Designation:''        
                  },
        Projectbooking :{
                        projectNo: '',
                        projectName: '',
                        projectLead:'',
                        IsDelete:false, 
                        validation: {
                            projectNo: null,
                            projectName: null,
                            projectLead:null
                      }
        }, 
        Eqipmentbooking :{
                        _id:'',
                        Equipment_Name: '',
                        Location_Id: '',
                        Min_Batch_Size:'',
                        Max_Batch_Size:'', 
                        SetUp_Time:'',
                        Process_Time:'', 
                        Cleaning_Time:'',
                        Status_Id:'', 
                        Category_Id:'', 
                        validation: {
                                    Equipment_Name: null,
                                    Location_Id: null,
                                    Min_Batch_Size:null,
                                    Max_Batch_Size:null  ,
                                    SetUp_Time:null  ,
                                    Process_Time:null  ,
                                    Cleaning_Time:null  ,
                                    Status_Id:null  ,
                                    Category_Id:null  ,
                                    IsEuipmentSelected:false
                        },
          LocationList: [{
          id: '',
          Desc: '' 
        }] ,
        EqipmentListData:[{
          id: '',
          Desc: '' 
        }],
        SelectedEquipmentList:[],
        showModal: false,
        showCalenderModel:false
        }, 
        tabIndex:0,
        isProjectBooking:true,
        isEquipmentBooking:false,
        isReviewBooking:false,    
        showModal: false,
        projectListData:[],
        SelectedProjectId:"",      
        SelectedLocationId:"",
        BookingStatus:{
          BookingNumber:""}
        
    }
  constructor(props:any)
  {
  super(props); 
  this.submit=this.submit.bind(this);   
  this.isFormInvalid=this.isFormInvalid.bind(this);   
  this.getDisabledClass=this.getDisabledClass.bind(this);   
  this.formProjectInputChange=this.formProjectInputChange.bind(this);
  this.saveProjectBooking=this.saveProjectBooking.bind(this);
  this.saveEquipmentBooking=this.saveEquipmentBooking.bind(this);
  this.saveReviewBooking=this.saveReviewBooking.bind(this);
  this.handleCloseModal=this.handleCloseModal.bind(this);
  this.handleOpenModal = this.handleOpenModal.bind(this);
  this.bindProject = this.bindProject.bind(this);
  this.formEuipmentLocationChange = this.formEuipmentLocationChange.bind(this);
  this.getSelectedEqipmentList=this.getSelectedEqipmentList.bind(this);
  this.handleEquipmentCloseModal=this.handleEquipmentCloseModal.bind(this);
  this.onSelectCalenderBooking=this.onSelectCalenderBooking.bind(this);
 
  }
  componentDidMount(){
  this.GetLocationListDetails();
  this.GetProjectListDetails();
  }
  validateProject()
  {
      const validation:any=this.state.Projectbooking.validation;
      const projectBooking:any=this.state.Projectbooking;
      let  valid = true; 
        //#region  Validate Eqipment
      if( validation.projectNo==="Can not be empty"||validation.projectNo=== null)
      {   
          valid = false; 
          validation.projectNo="Can not be empty";         
      }
      else
      {
        validation.projectNo="";
      }
      if(validation.projectName==="Can not be empty"||validation.projectName===null)
      {   
          valid = false;  
          validation.projectName="Can not be empty";        
      }
      else
      {
        validation.projectName="";
      }
      if(validation.projectLead==="Can not be empty"||validation.projectLead===null)
      {   
          valid = false; 
          validation.projectLead="Can not be empty";         
      }
      else
      {
        validation.projectLead="";
      }
      projectBooking.validation=validation;
      this.setState({
        Projectbooking:projectBooking
      });
       //#endregion 
      return valid;
  }
  saveProjectBooking(event:any)
  {
    event.preventDefault();
    if(this.validateProject())
      {
          if(this.state.SelectedProjectId!=="")
          {
            const projectBooking:any=this.state.Projectbooking;
            
            this.setState({
              showModal: false,
              isProjectBooking:false,
              isEquipmentBooking:true,
              isReviewBooking:false
            });  
          }
          else
          {
            const projectBooking:any=this.state.Projectbooking;   
            this.setState({
              Projectbooking:projectBooking,
              showModal: false,
              isProjectBooking:false,
              isEquipmentBooking:true,
              isReviewBooking:false
            });  

          }
               
     
      }
      else
      {
        return false;
      }
   
  }
  validateEquipment()
  {
    const validation:any=this.state.Eqipmentbooking.validation;
    const eqipmentBooking:any=this.state.Eqipmentbooking;

    let  valid = true; 
   //#region  Validate Eqipment
    // if( validation.Equipment_Name==="Can not be empty"||validation.Equipment_Name===null)
    //   {   
    //       valid = false; 
    //       validation.Equipment_Name="Can not be empty";        
    //   }
    //   else
    //   {
    //     validation.Equipment_Name="";   
    //   }
      if(validation.Location_Id==="Can not be empty"||validation.Location_Id===null)
      {   
          valid = false;
          validation.Location_Id="Can not be empty" ;    

      }
      else
      {
        validation.Location_Id="";   
      }
      if(validation.IsEuipmentSelected===false||validation.IsEuipmentSelected==="Can not be empty")
      {   
          valid = false;
          validation.IsEuipmentSelected="Can not be empty" ;    

      }
      else
      {
        validation.IsEuipmentSelected=true;   
      }
      // if(validation.Min_Batch_Size==="Can not be empty"||   validation.Min_Batch_Size===null)
      // {   
      //     valid = false;
      //     validation.Min_Batch_Size="Can not be empty" ;            
      // }
      // else
      // {
      //   validation.Min_Batch_Size="";   
      // }
      // if(validation.Max_Batch_Size==="Can not be empty"||validation.Max_Batch_Size===null)
      // {   
      //     valid = false;
      //     validation.Max_Batch_Size="Can not be empty"   ;          
      // }
      // else
      // {
      //   validation.Max_Batch_Size="";   
      // }
      // if( validation.SetUp_Time==="Can not be empty"||  validation.SetUp_Time===null)
      // {   
      //     valid = false;
      //     validation.SetUp_Time="Can not be empty"  ;           
      // }
      // else
      // {
      //   validation.SetUp_Time="";   
      // }
      // if(  validation.Process_Time==="Can not be empty"||validation.Process_Time===null)
      // {   
      //     valid = false;
      //     validation.Process_Time="Can not be empty"  ;           
      // }
      // else
      // {
      //   validation.Process_Time="";   
      // }
      // if(  validation.Cleaning_Time==="Can not be empty"||    validation.Cleaning_Time===null)
      // {   
      //     valid = false; 
      //     validation.Cleaning_Time="Can not be empty" ;           
      // }
      // else
      // {
      //   validation.Cleaning_Time="";   
      // }
      // if( validation.Status_Id==="Can not be empty"||validation.Status_Id===null)
      // {   
      //     valid = false; 
      //     validation.Status_Id="Can not be empty" ;           
      // }
      // else
      // {
      //   validation.Status_Id="";   
      // }
      // if( validation.Category_Id==="Can not be empty"||  validation.Category_Id===null)
      // {   
      //     valid = false;
      //     validation.Category_Id="Can not be empty";            
      // }
      // else
      // {
      //   validation.Category_Id="";   
      // }
    
     //#endregion 
     eqipmentBooking.validation=validation;
     this.setState({
      Eqipmentbooking:eqipmentBooking
     });
      return valid;
  }
  saveEquipmentBooking(event:any)
  {
    event.preventDefault();
    const eqipmentBooking:any=this.state.Eqipmentbooking;   
    
      if(this.validateEquipment())
      {

        
        let dataTemp:any[]=[];
        this.state.Eqipmentbooking.SelectedEquipmentList.forEach((element:any) => {
          let data={'CreateTime':new Date(),'UserId':'5f467d916f575d4168c11ea4',
          'EquipmentId':element._id,
          'StartTime':new Date("October 12 2020 16:30"),'EndTime':new Date("October 12 2020 18:30"),'RefNo':uuidv4()};
          dataTemp.push(data);
        });
        dataTemp.forEach((item:any)=>{
          axios.post('http://localhost:10024/api/TempBooking/Create', item).then(resp=>{
    
            // resp.data.forEach((item:any) => {
            //   tempNote.push({id: item._id,
            //       Desc: item.Equipment_Name ,
            //     data:item });
            // }); 
            // equipmentBooking.EqipmentListData= tempNote;   
            // this.setState({
            //   Eqipmentbooking:equipmentBooking,
      
            // });
            this.setState({
              Eqipmentbooking:eqipmentBooking,
              showModal: false,
              isProjectBooking:false,
              isEquipmentBooking:false,
              isReviewBooking:true
            });   
            }).catch(error => {
                console.log(error); 
                this.setState({
                  Eqipmentbooking:eqipmentBooking,
                  showModal: false,
                  isProjectBooking:false,
                  isEquipmentBooking:true,
                  isReviewBooking:false
                });   
              });
        })
        // this.setState({
        //   Eqipmentbooking:eqipmentBooking,
        //   showModal: false,
        //   isProjectBooking:false,
        //   isEquipmentBooking:false,
        //   isReviewBooking:true
        // });        
      }
      else
      {
        return false;
      }
       
  }
  saveReviewBooking(event:any)
  {
    event.preventDefault();
    let dataTemp:any[]=[];
    
    
    this.state.Eqipmentbooking.SelectedEquipmentList.forEach((element:any) => {
      
      let data={'Booking_No':uuidv4(),
    'Project_Id':this.state.SelectedProjectId,
    'User_Id':'5f467d916f575d4168c11ea4',
    'IsActive':null,
    'Trial_Objective':'test',
    'BookingDetails':{
                      'Equipment_id':element._id,
                      'BookingStartTime':new Date("October 12 2020 16:30"),
                      'BookingEndTime':new Date("October 12 2020 18:30"),
                      'Booking_Statusid':'0',
                      'Chkin_Status':'0',
                      'Chkin_Time':null,
                      'Chkout_status':'0',
                      'Chkout_Time':null    
                    } 
  };
    //   let data={'Equipment_id':element._id,
    //   'BookingStartTime':new Date("October 12 2020 16:30"),
    //   'BookingEndTime':new Date("October 12 2020 18:30"),
    //   'Booking_Statusid':'0',
    //   'Chkin_Status':'0',
    //   'Chkin_Time':null,
    //   'Chkout_status':'0',
    //   'Chkout_Time':null
      
    // };
      dataTemp.push(data);
    });
    dataTemp.forEach((item:any)=>{
      axios.post('http://localhost:10024/api/Booking/Create', item).then(resp=>{

    
      let responseData=resp.data;
      if(responseData.status && responseData.Message==="Booking Created Successfully")
      {
     
        const bookingStatus=this.state.BookingStatus;
        bookingStatus.BookingNumber=uuidv4();
        this.setState({  
          isProjectBooking:false,
          isEquipmentBooking:false,
          isReviewBooking:false,
          BookingStatus:bookingStatus
        }); 
      }
      else
      {
        this.setState({            
          showModal: false,
          isProjectBooking:false,
          isEquipmentBooking:false,
          isReviewBooking:true
        }); 
      }    

        
        }).catch(error => {
            console.log(error); 
            this.setState({            
              showModal: false,
              isProjectBooking:false,
              isEquipmentBooking:false,
              isReviewBooking:true
            });   
          });
    })

    // this.setState({
    // isProjectBooking:false,
    // isEquipmentBooking:false,
    // isReviewBooking:false
    // });
  }
  isFormInvalid(event:any){
      let valid = true;
      this.setState({
        [event.target.name]:event.target.value
      });
      const validation:any=this.state.validation;
    
      if(!event.target.value)
      {      
        valid = false;        
        validation[event.target.name]='Can not be empty';
      }
      else
      {
        validation[event.target.name]='';
      }
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

      if (event.target.name==='Email'&& (!pattern.test(event.target.value))) 
      {
        valid = false;   
          validation.Email = "Please enter valid email address.";
          //return false;
      } 
      else
      {
          validation.Email = "";
      }
      this.setState({
        validation
      });
      return valid ;

  }
  formEuipmentInputChange(event:any){
   
    const eqipmentBooking:any=this.state.Eqipmentbooking; 
    eqipmentBooking[event.target.name]=event.target.value;
 
    const validation:any=this.state.Eqipmentbooking.validation;
   
    if(!event.target.value)
    {   
        // valid = false;          
        validation[event.target.name]='Can not be empty';
    }
    else
    {
       validation[event.target.name]='';
    }
    eqipmentBooking.validation=validation;
    
    this.setState({
      Eqipmentbooking:eqipmentBooking
    });
    
    // return valid;
  }

  formProjectInputChange(event: any) { 
    const projectbooking:any=this.state.Projectbooking; 
    projectbooking[event.target.name]=event.target.value;
    
    const validation:any=this.state.Projectbooking.validation;
    if (!event.target.value) {
      validation[event.target.name] = 'Can\'t be empty';
    } else {
      validation[event.target.name] = '';
    }
    projectbooking.validation=validation;
    this.setState({
      Projectbooking:projectbooking
    });
  }
  submit(event:any){
    event.preventDefault();
   
    const validation:any=this.state.validation;
   
   //#region input validation   
     
    // //#endregion    
    
      if(validation.Name==='Can not be empty'||validation.Name==="")
      {
          validation.Name='Can not be empty';
          this.setState({
            validation
            });
             return false;
      }
      else if(validation.Designation==='Can not be empty'||validation.Designation==="")
      {
          validation.Designation='Can not be empty';
          this.setState({
          validation
          });
           return false;
      }
      else if(validation.Email==='Can not be empty'||validation.Email==="")
      {
          validation.Email='Can not be empty';
          this.setState({
            validation
            });
            return false;
      }
      else if(validation.Email==="Please enter valid email address.")
      {
          validation.Email='Please enter valid email address.';
          this.setState({
            validation
            });
            return false;
      }
      else if(
      validation.User_Password==='Can not be empty'||validation.User_Password==="")
      {
          validation.User_Password='Can not be empty';
          this.setState({
            validation
            });
            return false;
      }
   
    else
    {
        console.log("Validation Success");
       axios.post('http://localhost:10024/api/AppUser/SaveAppUserDetails', {
        'User_Password':this.state.User_Password,
        'uid':'',
        'Name':this.state.Name,
        'Designation':this.state.Designation,
        'Email':this.state.Email,
        "Profile_Image":'',
        'Channel_Id':'',
        'Nonce':'',
        'LocationId':1
        } ).then(res=>{
        console.log(res.data);
        let responseData=res.data;
        if(!responseData.status && responseData.Message==="User already exist")
        {
         alert("User already exist");
        }
        else if(responseData.status && responseData.Message==="User Created Successfully")
        {
          alert("User Created Successfully");
          // window.location.reload(false);
          window.location.href = "/login";
        }
        else  
        {
          alert("Some error Occured!!");
        }     
        
        }).catch(error => {
            console.log(error);
            alert("Some error Occured");
            // this.setState({
            //     message:"Server error.."
            // });
          });
    }

    // dispatch(login(formState.email.value));
    }
  getDisabledClass(event:any){
    let isError: boolean = this.isFormInvalid(event) as boolean;
    return isError ? "disabled" : "";
  }
  bindProject(event:any)
  {
    const projectBooking:any=this.state.Projectbooking;    
    projectBooking.projectNo =event.data.ProjectNo;
    projectBooking.projectName =event.data.ProjectName;
    projectBooking.projectLead =event.data.ProjectLead;
    projectBooking.IsDelete=event.data.IsDelete;
   
    const validation:any=this.state.Projectbooking.validation;
    validation.projectNo="";
    validation.projectName="";
    validation.projectLead="";
    projectBooking.validation=validation;
    
    this.setState({
      Projectbooking:projectBooking,
      SelectedProjectId:event.data._id,
      showModal: false
    });
  }
  GetLocationListDetails(){ 
    const equipmentBooking=this.state.Eqipmentbooking;
    let tempNote=equipmentBooking.LocationList;
    tempNote=[];
    console.log('GetLocationListDetails call');
     axios.post('http://localhost:10024/api/Location/GetLocationList', null).then(resp=>{
      //let responseData=resp.data;
     // alert(JSON.stringify( responseData)); 
      resp.data.forEach((item:any) => {
        tempNote.push({id: item._id,
            Desc: item.Desc  });
      });
      equipmentBooking.LocationList=tempNote;
      this.setState({
        Eqipmentbooking:equipmentBooking
      }) 
      }).catch(error => {
          console.log(error); 
        });
        
         
   }
  GetProjectListDetails()
  {
  axios.get('http://localhost:10024/api/project/getAllProject').then(res=>{
    console.log(res.data);
    if(res.data!==null && res.data.IsSuccess)
    {
        this.setState({
            projectListData:res.data.ResponseResult
        });
    }             
    
    }).catch(error => {
        console.log(error);
        
        // this.setState({
        //     message:"Server error.."
        // });
      });
  }
  formEuipmentLocationChange(event:any)
  {
      const locationId=event.target.value;
      let tempNote=this.state.Eqipmentbooking.EqipmentListData;
      tempNote=[];
      const equipmentBooking=this.state.Eqipmentbooking;
    
      if(locationId==="Select")
      {
         //#region making eqipmentList location wise empty as user select the 'select' option 
          equipmentBooking.EqipmentListData= tempNote;   
          this.setState({
            Eqipmentbooking:equipmentBooking,
          });
          //#endregion
          //#region removing already selected euipment         
          let selectedEquipmentList=this.state.Eqipmentbooking.SelectedEquipmentList;
          selectedEquipmentList=[];
          equipmentBooking.SelectedEquipmentList=selectedEquipmentList;
          this.setState({
            Eqipmentbooking:equipmentBooking
          });
          //#endregion
      }
      else
      {
          //#region  validation of locationId and assigning selected location id to state 
          const validation:any=this.state.Eqipmentbooking.validation;
          validation.Location_Id="";
          equipmentBooking.validation=validation;
          equipmentBooking.Location_Id=locationId;
          this.setState({
          Eqipmentbooking:equipmentBooking
          });
         //#endregion
          axios.post('http://localhost:10024/api/Equipment/GetEquipmentByLocationId', {'LocId':locationId}).then(resp=>{
    
          resp.data.forEach((item:any) => {
            tempNote.push({id: item._id,
                Desc: item.Equipment_Name ,
              data:item });
          }); 
          equipmentBooking.EqipmentListData= tempNote;   
          this.setState({
            Eqipmentbooking:equipmentBooking,
    
          });
          //#region removing already selected euipment
          const eqipmentData=this.state.Eqipmentbooking;
          let selectedEquipmentList=this.state.Eqipmentbooking.SelectedEquipmentList;
          selectedEquipmentList=[];
          eqipmentData.SelectedEquipmentList=selectedEquipmentList;
          this.setState({
            Eqipmentbooking:eqipmentData
          });
          //#endregion
          }).catch(error => {
              console.log(error); 
            });
      }
  }
  getSelectedEqipmentList(event:any)
  {
    const equipmentId=event.target.value;
    const equipmentBooking=this.state.Eqipmentbooking;   
    const EquipmentList=this.state.Eqipmentbooking.SelectedEquipmentList;
    const validation:any=this.state.Eqipmentbooking.validation; 
    if(equipmentId==="Select")
    {
      //#region  validation of Equipment Selecetion
      validation.IsEuipmentSelected=(EquipmentList.length>0?true:false);
      equipmentBooking.validation=validation;        
      this.setState({
       Eqipmentbooking:equipmentBooking
      });
        //#endregion
    }
    else
     {
        //#region  validation of Equipment Selecetion
        validation.IsEuipmentSelected=true;
        equipmentBooking.validation=validation;        
        this.setState({
        Eqipmentbooking:equipmentBooking
        });
        //#endregion
        const isEquipmentSelected=EquipmentList.filter((n:any)=>n._id===equipmentId);
        if(isEquipmentSelected.length===0)
        {
          axios.post('http://localhost:10024/api/Equipment/GetEquipmentById', {'Id':equipmentId}).then(resp=>{
        
            if(resp.data!=null)
            {
              const eqipmentData=this.state.Eqipmentbooking;
              const selectedEquipmentList=this.state.Eqipmentbooking.SelectedEquipmentList;
              console.log(resp.data);
              resp.data.forEach((item:any) => {
                selectedEquipmentList.push(item);
              });
              // selectedEquipmentList.push(resp.data);
              eqipmentData.SelectedEquipmentList=selectedEquipmentList;
              this.setState({
                Eqipmentbooking:eqipmentData
              });
            }
            }).catch(error => {
            console.log(error); 
            });
        }
        else
        {
          //  alert("Already Selected");
        }
     } 
  }
  deleteEquipment(id:string)
  {
    const eqipmentData=this.state.Eqipmentbooking;
    let selectedEquipmentList=this.state.Eqipmentbooking.SelectedEquipmentList;
    selectedEquipmentList=selectedEquipmentList.filter((n:any)=>n._id!==id);
    
    eqipmentData.SelectedEquipmentList=selectedEquipmentList;
    this.setState({
      Eqipmentbooking:eqipmentData
    });
  }
    
  handleCloseModal () {
    this.setState({ showModal: false });
  }
  handleOpenModal (event:any) {
    this.setState({ showModal: true });
  }
  OpenEquipmentSelectedModal(id:string)
  {
     
    const equipmentBooking=this.state.Eqipmentbooking;
    equipmentBooking.showModal=true;
    equipmentBooking.showModelEquipmentId=id;
    this.setState({ Eqipmentbooking:equipmentBooking });
  }
  handleEquipmentCloseModal(event:any)
  {
    const equipmentBooking=this.state.Eqipmentbooking;
    equipmentBooking.showModal=false;
    equipmentBooking.showModelEquipmentId="";
    this.setState({ Eqipmentbooking:equipmentBooking });
  }
  onSelectCalenderBooking(id:string)
  {
    const equipmentBooking=this.state.Eqipmentbooking;
    equipmentBooking.showCalenderModel=true;
    
    this.setState({ Eqipmentbooking:equipmentBooking });
  }
  render(){
    if(this.state.isProjectBooking)
    {
      return (

        <div className="container">
          <div className="">
            <div className="content user">
                    <div className="col-xl-7 col-lg-7">
                     <div className="card-header py-3">
                      <h6 className="m-0 font-weight-bold text-green">
                        Eqipment Booking</h6>
                     </div>
                     </div>
                     <div className="col-xl-7 col-lg-7">
                     <div className="card-header py-3">
                        <h6 className="m-0 font-weight-bold text-green">Booking Data</h6>
                        <div style={{justifyContent:'center',textAlign:'center'}}>
                           <span className="OrderStepsActive">1</span>-------------<span className="OrderSteps">2</span>-------------<span className="OrderSteps">3</span>
                         </div>
                     </div>
                     </div>
                <Projectbooking  Projectbooking={this.state.Projectbooking} handleOpenModal={(event)=>{this.handleOpenModal(event)}} formProjectInputChange={(event)=>{this.formProjectInputChange(event)}} saveProjectBooking={(event)=>{this.saveProjectBooking(event)}} />
               
            </div>
          </div>
          <ReactModal
          
          className="projectListModel"
          isOpen={this.state.showModal}
          contentLabel="Minimal Modal Example" >
          <div>
          <div className="col-12">
          <button onClick={this.handleCloseModal} >Close Modal</button>
          {/* className="ClodeBtnModel" */}
          </div>
          {  this.state.projectListData.map(lisdata =>{    
                              if(lisdata.ProjectNo!=null) 
                              {
                              return (
                              // <div className="row col col-12 text-center " key={lisdata._id} >
                              <ProjectList  data={lisdata} onSelectProject={this.bindProject} />
                              // </div> 
           )}})} 
         </div>
        </ReactModal>
        </div>
      );      
    }
    else if(this.state.isEquipmentBooking)
    {
      return(
        <div className="container">
         <div className="">
         <div className="content user">
         <div className="col-xl-7 col-lg-7">
                     <div className="card-header py-3">
                      <h6 className="m-0 font-weight-bold text-green">
                        Eqipment Booking</h6>
                     </div>
                     </div>
                     <div className="col-xl-7 col-lg-7">
                     <div className="card-header py-3">
                        <h6 className="m-0 font-weight-bold text-green">Booking Data</h6>
                        <div style={{justifyContent:'center',textAlign:'center'}}>
                           <span className="OrderSteps">1</span>-------------<span className="OrderStepsActive">2</span>-------------<span className="OrderSteps">3</span>
                         </div>
                     </div>
                     </div>
                     
                     <EqipmentBooking  Eqipmentbooking={this.state.Eqipmentbooking} 
                     formEuipmentInputChange={(event)=>this.formEuipmentInputChange(event)}
                      saveEquipmentBooking={(event)=>{this.saveEquipmentBooking(event)}}
                       formEuipmentLocationChange={(event)=>{this.formEuipmentLocationChange(event)}} 
                       getSelectedEqipmentList={(event)=>{this.getSelectedEqipmentList(event)}} 
                       deleteEquipment={(id)=>this.deleteEquipment(id)}
                       OpenEquipmentSelectedModal={(id)=>{this.OpenEquipmentSelectedModal(id)}} 
                       handleEquipmentCloseModal={(event)=>{this.handleEquipmentCloseModal(event)}}
                       onSelectCalenderBooking={(id)=>{this.onSelectCalenderBooking(id)}} 
                       />
                       
           </div>

         
        

         </div>
        <div>
        
        </div>
     </div>
      );
    }
    else if(this.state.isReviewBooking)
    {
      return(
        <div className="container">
           <div className="">
               <div className="content user">
                    <div className="col-xl-7 col-lg-7">
                     <div className="card-header py-3">
                      <h6 className="m-0 font-weight-bold text-green">
                        Eqipment Booking</h6>
                     </div>
                     </div>
                     <div className="col-xl-7 col-lg-7">
                     <div className="card-header py-3">
                        <h6 className="m-0 font-weight-bold text-green">Booking Data</h6>
                        <div style={{justifyContent:'center',textAlign:'center'}}>
                           <span className="OrderSteps">1</span>-------------<span className="OrderSteps">2</span>-------------<span className="OrderStepsActive">3</span>
                         </div>
                          </div>
                     </div>
                       
                      <Reviewbooking  Projectbooking={this.state.Projectbooking} 
                      SelectedEquipmentList={this.state.Eqipmentbooking.SelectedEquipmentList} 
                      SelectedLocationId={this.state.SelectedLocationId} 
                      SelectedProjectId={this.state.SelectedProjectId}
                      saveReviewBooking={this.saveReviewBooking}
                      />  
                    
                </div>
         
            </div>
        <div>
        {/* <button id="Save" onClick={this.saveReviewBooking}>Next</button> */}
        {/* <button  className={`btn btn-info `}type="submit" onClick={this.saveReviewBooking}>
          Next
        </button> */}
        </div>
     </div>
      );
    }   
    else{
     return (
      <BookingStatus BookingStatus={this.state.BookingStatus} />
     );
    
      
  }
}

}
