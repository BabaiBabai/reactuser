import React from "react"; 
import axios from 'axios'; 
 
import "../Master/css/Project.css";
 
// export default class ProjectForm extends Component {
    export interface IReviewbookingProp
    {
                Projectbooking :{
                                    projectNo: '',
                                    projectName: '',
                                    projectLead:'',
                                    IsDelete:false
                            },
                
                SelectedEquipmentList:[],
                SelectedProjectId:string,      
                SelectedLocationId:string ,           
                saveReviewBooking(event:any):any
    }   
const Reviewbooking=(props:IReviewbookingProp)=>{
     return (
        
         <div className="col-xl-7 col-lg-7">         
                 <div className="card mb-4">
                     <div className="card-header py-3">
             <h6 className="m-0 font-weight-bold text-green">Review Booking Details</h6>
            
             {/* <a style={{cursor:'pointer',color: 'blue',float: 'right'}} onClick={(event)=>{props.handleOpenModal(event)}}>Explore Project list </a> */}
           </div>
                          <div className="card-body"> 
                               <form onSubmit={()=>{}}>
                                   <div className="row"  style={{margin: 0}}>
                                        <div className="col-4"> 
                                            <div className="row"  style={{margin: 0}}>
                                               <div className="col-12">
                                                    <label><b>Project Number</b></label>
                                               </div>
                                                <div className="col-12">
                                                {props.Projectbooking.projectNo}
                                                </div>  
                                              </div>
            
                                        </div>   
                                        <div className="col-4"> 
                                            <div className="row"  style={{margin: 0}}>
                                               <div className="col-12">
                                                    <label><b>Project Name</b></label>
                                               </div>
                                                <div className="col-12">
                                                {props.Projectbooking.projectName}
                                                </div>  
                                            </div>
                                        </div> 
                                        <div className="col-4">
                                             <div className="row"  style={{margin: 0}}>
                                               <div className="col-12">
                                                    <label><b>Project Lead</b></label>
                                               </div>
                                                <div className="col-12">
                                                {props.Projectbooking.projectLead}
                                                </div>  
                                            </div>
                                        </div> 
                                    </div>
                                  <div className="row"  style={{margin: 0}}>
                                       <div className="col-4"> 
                                            <div className="row"  style={{margin: 0}}>
                                               <div className="col-12">
                                                    <label><b>Project Number</b></label>
                                               </div>
                                                <div className="col-12">
                                                {props.Projectbooking.projectNo}
                                                </div>  
                                              </div>
            
                                        </div>                                                  
                                   </div>
                                   {props.SelectedEquipmentList.map((item:any)=>{
                                       return(
                                        <div className="row"  style={{margin: 0, border: '1px solid rgba(0,0,0,.125)',borderRadius:'.25rem'}} key={item._id}>
                                            <div className="col-6"> 
                                                <div className="row"  style={{margin: 0}}>
                                                    <div className="col-12">
                                                        <label><b>Equipment id</b></label>
                                                    </div>
                                                    <div className="col-12">
                                                    {item._id}
                                                    </div>  
                                                </div>
                                            </div> 
                                            <div className="col-6"> 
                                                <div className="row"  style={{margin: 0}}>
                                                    <div className="col-12">
                                                        <label><b>Booking Slot</b></label>
                                                    </div>
                                                    <div className="col-12">
                                                    On {new Date().toLocaleDateString()}
                                                    </div>  
                                                </div>
                                            </div>                                                  
                                      </div>
                                       );
                                    
                                   
                                }
                                   )}
                                  
           <div className="form-group text-center">
               <button type="submit" className='btn btn-info' style={{marginTop:'10px'}} onClick={props.saveReviewBooking}>Save</button>   
             
           </div>
         </form>
         </div>
         </div>
       </div> 
     );
 }

 export default Reviewbooking;

 
  