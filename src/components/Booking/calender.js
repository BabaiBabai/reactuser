import React, { Component,Children } from 'react'
import {Calendar,momentLocalizer} from 'react-big-calendar'
import moment from 'moment'
import TimePicker from 'react-time-picker';

 
//import 'react-big-calendar/lib/sass/styles';
//import 'react-big-calendar/lib/addons/dragAndDrop/styles';
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer. momentLocalizer
const localizer =momentLocalizer(moment) // or globalizeLocalizer
let ss=new Date();
const eventss = [
  {
      
    'title': 'ss',
    //'allDay': true,
    'start': new Date(),
    'end': new Date(),
    'isColor':1,
    'ismaxCount':1
  },
  {
    'title': 'Long Event',
    'start': new Date(2020, 8, 7),
    'end': new Date(2020, 8 ,10),
    'isColor':1,
    'ismaxCount':1
  },

  {
    'title': 'DTS STARTS',
    'start': new Date(2020, 2, 13, 0, 0, 0),
    'end': new Date(2020, 2, 20, 0, 0, 0),
    'isColor':2,
    'ismaxCount':2
  },

  {
    'title': 'DTS ENDS',
    'start': new Date(2016, 10, 6, 0, 0, 0),
    'end': new Date(2016, 10, 13, 0, 0, 0),
    'isColor':2,
    'ismaxCount':2
  },

  {
    'title': 'Some Event',
    'start': new Date(2017, 3, 9, 0, 0, 0),
    'end': new Date(2017, 3, 9, 0, 0, 0),
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Conference',
    'start': new Date(2017, 3, 11),
    'end': new Date(2017, 3, 13),
    desc: 'Big conference for important people',
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Meeting',
    'start': new Date(2017, 3, 12, 10, 30, 0, 0),
    'end': new Date(2017, 3, 12, 12, 30, 0, 0),
    desc: 'Pre-meeting meeting, to prepare for the meeting',
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Lunch',
    'start':new Date(2017, 3, 12, 12, 0, 0, 0),
    'end': new Date(2017, 3, 12, 13, 0, 0, 0),
    desc: 'Power lunch',
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Meeting',
    'start':new Date(2017, 3, 12,14, 0, 0, 0),
    'end': new Date(2017, 3, 12,15, 0, 0, 0),
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Happy Hour',
    'start':new Date(2017, 3, 12, 17, 0, 0, 0),
    'end': new Date(2017, 3, 12, 17, 30, 0, 0),
    desc: 'Most important meal of the day',
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Dinner',
    'start':new Date(2017, 3, 12, 20, 0, 0, 0),
    'end': new Date(2017, 3, 12, 21, 0, 0, 0),
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Birthday Party',
    'start':new Date(2017, 3, 13, 7, 0, 0),
    'end': new Date(2017, 3, 13, 10, 30, 0),
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Late Night Event',
    'start':new Date(2020, 3, 17, 19, 30, 0),
    'end': new Date(2020, 3, 18, 2, 0, 0),
    'isColor':2,
    'ismaxCount':2
  },
  {
    'title': 'Multi-day Event',
    'start':new Date(2020, 3, 20, 19, 30, 0),
    'end': new Date(2020, 3, 22, 2, 0, 0),
    'isColor':2,
    'ismaxCount':2
  }
]
let myEventsList=[];
 
const MyCalendar = props => (
  <div>
<TimePicker  className="form-control" />

  <div style={{ height: 500 }}>
    <Calendar
    
    selectable
      localizer={localizer}
      events={eventss} 
      startAccessor="start"
      endAccessor="end"
      //isColor="isColor" 
      // onSelectSlot={(this.slotSelected)}
      // onSelectEvent={(this.eventSelected)}
      //eventPropGetter
      //onSelectDate
      onSelectSlot={slotInfo =>
        alert(
          `selected slot: \n\nstart ${slotInfo.start.toLocaleString()} ` +
          `\nend: ${slotInfo.end.toLocaleString()}` +
          `\naction: ${slotInfo.action}`
        )
      }
      eventPropGetter={
        (event, start, end, isSelected) => {
          let newStyle = {
            backgroundColor: "lightgrey",
            color: 'black',
            borderRadius: "0px",
            border: "none"
          };
    
          // if (event.isMine){
          //   newStyle.backgroundColor = "lightgreen"
          // }
          if (event.isColor){
            newStyle.backgroundColor = "lightgreen"
          }
    
          return {
            className: "ffdddd",
            style: newStyle
          };
        }
      }
      // components={{
      //   month: {
      //     dateHeader: (props) => (
      //       <div>{props.Date}</div>
      //     )
      //   }
      // }}
    //   components={{
         
    //     dateCellWrapper: ColoredDateCellWrapper,
        
    //     month: {
    //             dateHeader: CustomDateHeader
    //         },
    // }}
    onNavigate={(day) => {
      alert(day)
    }}
      components={{
        month: {
            dateHeader: CustomDateHeader 

        },
        //dateCellWrapper: { eventPropGetter: ColoredDateCellWrapper},
        day:{
          header: CustomDateHeader1
        } 
      }}
      // components={{
      //   month: {
          
      //     dateHeader: ({ date, label }) => {
      //       let highlightDate =
      //       // eventss.find(event =>
      //       //     moment(date).isBetween(
      //       //       moment(event.startDate),
      //       //       moment(event.endDate),
      //       //       null,
      //       //       "[]"
      //       //     )
      //       //   ) != undefined;
      //       eventss.find(e=>e.ismaxCount===1 )
      //       return (
      //         <h1 style={highlightDate ? { color: "red" } : { color: "blue" }}>{label}</h1>
      //       );
      //     }
      //   }
      // }}
    />
  </div>
  </div>
)
const onNavigate = (date, view, action) => {
  alert(date)
}
const ColoredDateCellWrapper = ({children, value,isColor}) => 
    React.cloneElement(Children.only(children), {
    
        style: {
            ...children.style,
           // backgroundColor: value < CURRENT_DATE ? 'lightgray' : 'lightblue', 
           backgroundColor: isColor==='1'? 'lightgray' : 'lightblue', 
        },
    });

const CURRENT_DATE = moment().toDate();
// function ColoredDateCellWrapper({ children, value }) {
//   return (
//     React.cloneElement(children.only(children), {
//       style: {
//           ...children.style,
//           backgroundColor: value < CURRENT_DATE ? 'lightgreen' : 'lightblue',
//       },
//   })
//   )
  
// }
function CustomDateHeader({ label, drilldownView, onDrillDown }) {
  //let isVisible=value<CURRENT_DATE?'1':'2';
  return (

      <span > 
          <a href="#" onClick={onDrillDown}>
              {/* {label} */}
              {label}
          </a>
          {/* <button>Book Class</button> */}
      </span>
  )
  
}

function CustomDateHeader1({ label, drilldownView, onDrillDown }) {
  return (
      <span>
          {/* <a href="" onClick={onDrillDown}> */}
          <a href="" onClick={clickHandler}>
              {label}
          </a>
          {/* <button>Book Class</button> */}
      </span>
  )
  
}
function clickHandler(){
  console.log('I am clickHandler');

}

export default MyCalendar