
    
import React, { Component,Children } from 'react'
import {Calendar,momentLocalizer} from 'react-big-calendar'
import moment from 'moment'
import TimePicker from 'rc-time-picker';
// import ReactDOM from 'react-dom';
import 'rc-time-picker/assets/index.css';
 //import TimePicker from 'react-time-picker';
import DatePicker from "react-datepicker";
 


export default class BookingCalender extends Component {
 
  state = {
    StartTime:'',
    EndTime:'',
    validation: {
        StartTime: null ,
        EndTime: null  
    } ,
    NextButtonDisable:false,
    SelectedBookingdate:null,
    StartDateOfCalender:new Date(),
    EndDateOfCalender:new Date(),
    TimePickerDiv:false,
    CalenderDiv:true,
    eventss:[{ 
        'title': 'ss',
        //'allDay': true,
        'start': new Date(),
        'end': new Date(),
        'isColor':1,
        'ismaxCount':1
      },
      {
        'title': 'Long Event',
        'start': new Date(2020, 8, 7),
        'end': new Date(2020, 8 ,10),
        'isColor':1,
        'ismaxCount':1
      },]
  };
  constructor(props:any) {
    super(props);
    this.SetBookingTime = this.SetBookingTime.bind(this);
    this.BackToCalender = this.BackToCalender.bind(this);
    this.BookingdateChange = this.BookingdateChange.bind(this);
  } 
  SetBookingTime(){
    this.setState({
        TimePickerDiv:true,
        CalenderDiv:false,
        NextButtonDisable:false
    })
  }
  BackToCalender(){
    this.setState({
        TimePickerDiv:false,
        CalenderDiv:true,
        NextButtonDisable:true
    })
  }
  formSubmit(event: any) {
    event.preventDefault();
  }
  // handleChange = date => {
    
  // };
  BookingdateChange (date:any) {
    
  };

  render() {
    const localizer =momentLocalizer(moment);
    
     
    return (
        <React.Fragment>
        <div style={{display: this.state.CalenderDiv ? 'block' : 'none' }}>
        <div style={{ height: 500 ,}} >
         <Calendar 
            selectable
            localizer={localizer}
            events={this.state.eventss} 
            startAccessor="start"
            endAccessor="end"
            onNavigate={(date,view) => { 
               this.setState({ 
                SelectedBookingdate:date
               })
               let start, end;  
                if (view === 'month') {
                    start = moment(date).startOf('month').startOf('week')
                    end = moment(date).endOf('month').endOf('week')
                    this.setState({
                        StartDateOfCalender:start,
                        EndDateOfCalender:end
                    })
             }
             console.log(start +' dfd '+end);
               
            }} 
            onView={(view) => { 
                if(view==='day'){
                    this.setState({
                        NextButtonDisable:true 
                       })      
                }
                else{
                    this.setState({
                        NextButtonDisable:false 
                       })  
                } 
              }}
              //min={new Date()}
        />
      </div> 
      
      <div style={{display: this.state.NextButtonDisable ? 'block' : 'none' }}>
          <button type='button' className='btn btn-info' onClick={this.SetBookingTime}>Next</button>
      </div>

      </div>
      <div style={{display: this.state.TimePickerDiv ? 'block' : 'none' }}>

      <div className="col-xl-7 col-lg-7">
        <div className="card shadow mb-4">
          <div className="card-header py-3">
            <h6 className="m-0 font-weight-bold text-green">Set Time</h6>
          </div>
          <div className="card-body"> 

      <form onSubmit={this.formSubmit}>
      <div className="form-group">
            <label>Booking Date</label>
            <DatePicker
                
                selected={this.state.SelectedBookingdate}
                onChange={this.BookingdateChange}
                dateFormat={"dd MMMM yyyy"}
            />
            
          </div>
          <div className="form-group">
            <label>StartTime</label>
            <TimePicker showSecond={false} minuteStep={15} use12Hours={true} />
            {this.state.validation.StartTime ? <span className="error title-error" style={{ color: 'red' }}>{this.state.validation.StartTime}</span> : null}
          </div>
          <div className="form-group">
            <label>EndTime</label>
            <TimePicker  showSecond={false} minuteStep={15} use12Hours={true} />
            {this.state.validation.EndTime ? <span className="error title-error" style={{ color: 'red' }}>{this.state.validation.EndTime}</span> : null}
          </div>
          <div className="form-group">
            <button type="submit" className='btn btn-info' style={{marginTop:'10px'}}>Save</button>
           
          </div>
          
        </form>
              <button className='btn btn-danger' type='button' onClick={this.BackToCalender}>Back</button>
      </div>
      </div>
      </div>
      </div>
      </React.Fragment>
    );
  }
}