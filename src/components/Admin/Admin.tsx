import React, { Fragment } from "react";
import LeftMenu from "../LeftMenu/LeftMenu";
import TopMenu from "../TopMenu/TopMenu";
import { Switch, Route } from "react-router";
import Users from "../Users/Users";
import Products from "../Products/Products";
import Orders from "../Orders/Orders";
import Home from "../Home/Home";
import Equipment from "../Master/EquipmentForm";
import Notifications from "../../common/components/Notification";
import ProjectForm from "../Master/ProjectForm";
import ProjectList from "../Master/MasterDataList/ProjectList";
import LocationForm from '../Master/LocationForm';
import Calender from '../Booking/calender';
import CalenderNew from '../Booking/calenderNew';
import OrderBooking from "../Booking/OrderBooking";

const Admin: React.FC = () => {

  return (
    <Fragment>
      <Notifications />
      <LeftMenu />
      <div id="content-wrapper" className="d-flex flex-column">
        <div id="content">
          <TopMenu />
          <div className="container-fluid">
            <Switch>
              <Route path={`/users`}><Users /></Route>
              <Route path={`/products`}><Products /></Route>
              <Route path={`/orders`}><Orders /></Route>
              <Route path={`/EquipmentForm`}><Equipment /></Route>
              <Route path={`/ProjectForm`}><ProjectForm /></Route>
              <Route path={`/LocationForm`}><LocationForm /></Route>
              <Route path={`/Calender`}><CalenderNew /></Route>
              {/* <Route path={`/Calender`}><Calender /></Route> */}
              <Route path={`/Booking`}><OrderBooking /></Route>
              {/* <Route path={`/ProjectList`}><ProjectList /></Route> */}
              <Route path="/"><Home /></Route>
              
            </Switch>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Admin;
