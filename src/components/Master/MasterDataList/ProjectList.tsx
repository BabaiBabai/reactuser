import React from "react";
// import Collapsible from 'react-collapsible';

// import '../css/note.css';

export interface IProjectListProp
{
    data:{
         
    ProjectNo: '',
    ProjectName: '',
    ProjectLead:'',
    IsDelete:false 
    } ,
 
    // onSelectProject(props:any):any
    onSelectProject(event:any):any
}
const ProjectList=(props:IProjectListProp)=>{
//can right here logic
 console.log("IProjectListProp"+props);
    return(
        
                <div className="row col col-12 shadow content user mb-4 ml-4 mr-4 " >
                    <div className="col-8"> 
                    <div className="row"  style={{margin: 0}}>
                    
                        <div className="col-8">
                          <label>Project Id</label>
                        </div>
                        <div className="col-2">
                          {props.data.ProjectNo}
                          </div>                        
                    </div>
                    <div className="row" style={{margin: 0}}>
                   
                        <div className="col-8">
                        <label>Project Name</label>
                        </div>
                        <div className="col-2">
                          {props.data.ProjectName} 
                          </div>                        
                    </div>
                    <div className="row" style={{margin: 0}}>
                        <div className="col-8">
                        <label>Project Lead</label>
                        </div>
                        <div className="col-2">
                           {props.data.ProjectLead} 
                         </div>                        
                    </div>
                      </div>
                    <div className="col-4">
                        {/* <button onClick={props.onSelectProject(props)}>Select</button> */}
                        <button className="btn btn-info mt-4" onClick={()=>props.onSelectProject(props)}>Select</button>
                     </div>
                    <br/>
                </div>

    );
    //functional componrnt support props
}
export default ProjectList;
//below is the class component

// export default class Note extends Component<INoteProp>
// {
//     render(){
//            return(
//             <div className="col-6 mt-5">
//             <div className="card">
//             <div className="card-header">
//               <h1>{this.props.note.title}</h1>
//             </div>
//             <div className="card-body">
//             <h1>{this.props.note.body}</h1>
//             </div>
//             </div>

// </div>

//            );
//     }
// }