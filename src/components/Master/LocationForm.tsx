import React, { Component } from "react"; 
import axios from 'axios';  
export default class LocationForm extends Component {
 
  state = {
    LocationDesc: '' ,
    validation: {
        LocationDesc: null 

    } 
  };

  constructor(props: any) {
    super(props);
    
    this.formInputChange = this.formInputChange.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }
   
 

  formInputChange(event: any) {  
    this.setState({
      [event.target.name]: event.target.value
    });

    const validation: any = this.state.validation;
    if (!event.target.value) {
      validation[event.target.name] = 'Can\'t be empty';
    } else {
      validation[event.target.name] = '';
    }
    this.setState({
      validation
    });
  }
  
  CheckAllValidation(event:any){ 
    let valid = true;
    const validation: any = this.state.validation;
     
      if(!event.target.LocationDesc.value)
      {   
         valid = false;        
         validation[event.target.LocationDesc.name]='Can not be empty';
      }     
      
  this.setState({
    validation
  });
   return valid
  }


  formSubmit(event: any) {
    event.preventDefault();
    if(this.CheckAllValidation(event))
    {
        let LocationInputModel ={
          Desc:event.target.LocationDesc.value
        }
        axios.post('http://localhost:10024/api/Location/CreateLocation', LocationInputModel ).then(res=>{
             console.log(res.data);
             let responseData=res.data;
             if(responseData.IsSuccess)
             {
              alert(responseData.Message);
              this.setState({ 
                LocationDesc: '' 
              });

             } 
            else
            {
              alert(responseData.Message);
            }     
            
            }).catch(error => {
                console.log(error);
                alert("Some error Occured"); 
              });

    }
    
  }
  
  render() {
   
    return (
        <div className="col-xl-7 col-lg-7">
        <div className="card shadow mb-4">
          <div className="card-header py-3">
            <h6 className="m-0 font-weight-bold text-green">Location</h6>
          </div>
          <div className="card-body"> 
        <form onSubmit={this.formSubmit}>
          <div className="form-group">
            <label>Equipment_Name</label>
            <input type="text" name="LocationDesc" placeholder="Location" className="form-control" value={this.state.LocationDesc} onChange={this.formInputChange} />
            {this.state.validation.LocationDesc ? <span className="error title-error" style={{ color: 'red' }}>{this.state.validation.LocationDesc}</span> : null}
          </div>
          
          <div className="form-group">
            <button type="submit" className='btn btn-info' style={{marginTop:'10px'}}>Save</button>
           
          </div>
          
        </form>
        </div>
        </div>
      </div> 
    );
  }
}