import React, { Component } from "react"; 
import axios from 'axios'; 
import { Link } from 'react-router-dom'; 
export default class EquipmentForm extends Component {
 
  state = {
    Equipment_Name: '',
    Location_Id: '',
    Min_Batch_Size:'',
    Max_Batch_Size:'', 
    SetUp_Time:'',
    Process_Time:'', 
    Cleaning_Time:'',
    Status_Id:'', 
    Category_Id:'', 
    validation: {
      Equipment_Name: null,
      Location_Id: null,
      Min_Batch_Size:null,
      Max_Batch_Size:null  ,
      SetUp_Time:null  ,
      Process_Time:null  ,
      Cleaning_Time:null  ,
      Status_Id:null  ,
      Category_Id:null  ,

    }, 
    LocationList: [{
        id: '',
        Desc: '' 
      },
    ] 
  };

  constructor(props: any) {
    super(props);
    
    this.formInputChange = this.formInputChange.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }
  componentDidMount(){ 
    this.GetLocationListDetails();
  }
 

  formInputChange(event: any) {  
    this.setState({
      [event.target.name]: event.target.value
    });

    const validation: any = this.state.validation;
    if (!event.target.value) {
      validation[event.target.name] = 'Can\'t be empty';
    } else {
      validation[event.target.name] = '';
    }
    this.setState({
      validation
    });
  }
  
  CheckAllValidation(event:any){ 
    let valid = true;
    const validation: any = this.state.validation;
     
      if(!event.target.Equipment_Name.value)
      {   
         valid = false;        
         validation[event.target.Equipment_Name.name]='Can not be empty';
      }     
      if(!event.target.Location_Id.value)
      {  
          valid = false;         
         validation[event.target.Location_Id.name]='Can not be empty';
      }
      if(!event.target.Min_Batch_Size.value)
      {    
          valid = false;       
         validation[event.target.Min_Batch_Size.name]='Can not be empty';
      }
      if(!event.target.Max_Batch_Size.value)
      {    
          valid = false;       
         validation[event.target.Max_Batch_Size.name]='Can not be empty';
      }
      if(!event.target.SetUp_Time.value)
      {    
          valid = false;       
         validation[event.target.SetUp_Time.name]='Can not be empty';
      }
      if(!event.target.Process_Time.value)
      {    
          valid = false;       
         validation[event.target.Process_Time.name]='Can not be empty';
      }
      if(!event.target.Cleaning_Time.value)
      {    
          valid = false;       
         validation[event.target.Cleaning_Time.name]='Can not be empty';
      }
      if(!event.target.Status_Id.value)
      {    
          valid = false;       
         validation[event.target.Status_Id.name]='Can not be empty';
      }
      if(!event.target.Category_Id.value)
      {    
          valid = false;       
         validation[event.target.Category_Id.name]='Can not be empty';
      }
  this.setState({
    validation
  });
   return valid
  }


  formSubmit(event: any) {
    event.preventDefault();
    if(this.CheckAllValidation(event))
    {
        let EquipmentInputModel ={
          Equipment_Name:event.target.Equipment_Name.value,
          Location_Id:event.target.Location_Id.value,
          Min_Batch_Size:event.target.Min_Batch_Size.value,
          Max_Batch_Size:event.target.Max_Batch_Size.value,
          SetUp_Time:event.target.SetUp_Time.value,
          Process_Time:event.target.Process_Time.value,
          Cleaning_Time:event.target.Cleaning_Time.value,
          Status_Id:event.target.Status_Id.value,
          Category_Id:event.target.Category_Id.value,
          //IsDecommission:false,
          Comments:'', 
          //IsDelete:false,
        }
        axios.post('http://localhost:10024/api/Equipment/CreateEquipment', EquipmentInputModel ).then(res=>{
             console.log(res.data);
             let responseData=res.data;
             if(responseData.IsSuccess)
             {
              alert(responseData.Message);
              this.setState({ 
                Equipment_Name: '',
                Location_Id: '',
                Min_Batch_Size:'',
                Max_Batch_Size:'',
                SetUp_Time:'',
                Process_Time:'',
                Cleaning_Time:'',
                Status_Id:'',
                Category_Id:''
              });

             } 
            else
            {
              alert(responseData.Message);
            }     
            
            }).catch(error => {
                console.log(error);
                alert("Some error Occured"); 
              });

    }
    
  }
   GetLocationListDetails(){ 
    let tempNote=this.state.LocationList;
    tempNote=[];
    console.log('GetLocationListDetails call');
     axios.post('http://localhost:10024/api/Location/GetLocationList', null).then(resp=>{
      //let responseData=resp.data;
     // alert(JSON.stringify( responseData)); 
      resp.data.forEach((item:any) => {
        tempNote.push({id: item._id,
            Desc: item.Desc  });
      });
      this.setState({
        LocationList:tempNote
      }) 
      }).catch(error => {
          console.log(error); 
        });
        
         
   }
  render() {
   
    return (
        <div className="col-xl-7 col-lg-7">
        <div className="card shadow mb-4">
          <div className="card-header py-3">
            <h6 className="m-0 font-weight-bold text-green">Equipment</h6>
          </div>
          <div className="card-body"> 
        <form onSubmit={this.formSubmit}>
          <div className="form-group">
            <label>Equipment_Name</label>
            <input type="text" name="Equipment_Name" placeholder="Equipment_Name" className="form-control" value={this.state.Equipment_Name} onChange={this.formInputChange} />
            {this.state.validation.Equipment_Name ? <span className="error title-error" style={{ color: 'red' }}>{this.state.validation.Equipment_Name}</span> : null}
          </div>
          <div className="form-group">
          <label>Location</label>
          <select name='Location_Id' className="form-control error">
                  {this.state.LocationList.map(item => (
                   <option  key={item.id} value={item.id} > {item.Desc} </option>
                  ))}
                </select>
          </div>
          <div className="form-group">
          <label>Min_Batch_Size</label>
            <input type="number" name="Min_Batch_Size" placeholder="Min_Batch_Size" className="form-control" value={this.state.Min_Batch_Size} onChange={this.formInputChange} />
            {this.state.validation.Min_Batch_Size ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.Min_Batch_Size}</span> : null}
          </div>
          <div className="form-group">
          <label>Max_Batch_Size</label>
            <input type="number" name="Max_Batch_Size" placeholder="Max_Batch_Size" className="form-control" value={this.state.Max_Batch_Size} onChange={this.formInputChange} />
            {this.state.validation.Max_Batch_Size ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.Max_Batch_Size}</span> : null}
          </div>
          <div className="form-group">
          <label>SetUp_Time</label>
            <input type="number" name="SetUp_Time" placeholder="SetUp_Time" className="form-control" value={this.state.SetUp_Time} onChange={this.formInputChange} />
            {this.state.validation.SetUp_Time ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.SetUp_Time}</span> : null}
          </div>
          
          <div className="form-group">
          <label>Process_Time</label>
            <input type="number" name="Process_Time" placeholder="Process_Time" className="form-control" value={this.state.Process_Time} onChange={this.formInputChange} />
            {this.state.validation.Process_Time ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.Process_Time}</span> : null}
          </div>
          <div className="form-group">
          <label>Cleaning_Time</label>
            <input type="number" name="Cleaning_Time" placeholder="Cleaning_Time" className="form-control" value={this.state.Cleaning_Time} onChange={this.formInputChange} />
            {this.state.validation.Cleaning_Time ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.Cleaning_Time}</span> : null}
          </div>
          <div className="form-group">
          <label>Status_Id</label>
            <input type="text" name="Status_Id" placeholder="Status_Id" className="form-control" value={this.state.Status_Id} onChange={this.formInputChange} />
            {this.state.validation.Status_Id ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.Status_Id}</span> : null}
          </div>
          <div className="form-group">
          <label>Category_Id</label>
            <input type="text" name="Category_Id" placeholder="Category_Id" className="form-control" value={this.state.Category_Id} onChange={this.formInputChange} />
            {this.state.validation.Category_Id ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.Category_Id}</span> : null}
          </div>

          
           
   
          <div className="form-group">
            <button type="submit" className='btn btn-info' style={{marginTop:'10px'}}>Save</button>
           
          </div>
          
        </form>
        </div>
        </div>
      </div> 
    );
  }
}