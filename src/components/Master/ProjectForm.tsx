import React, { Component } from "react"; 
import axios from 'axios'; 
 
import "./css/Project.css";
import { Link } from "react-router-dom";
import ProjectList from "./MasterDataList/ProjectList";
import Collapsible from "react-collapsible";

export default class ProjectForm extends Component {

  state :{projectNo:string,projectName:string,projectLead:string,IsDelete:boolean,validation:any,isUpdate:boolean,buttonName:string,projectListData:any[]}={
    projectNo: '',
    projectName: '',
    projectLead:'',
    IsDelete:false, 
    validation: {
        projectNo: null,
        projectName: null,
        projectLead:null
        //,
       // IsDelete:null
    },    
    isUpdate:false,
    buttonName:'Save',
    projectListData:[]
   
  };

  constructor(props: any) {
    super(props);
    this.formInputChange = this.formInputChange.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }
  componentDidMount(){
    axios.get('http://localhost:10024/api/project/getAllProject').then(res=>{
         console.log(res.data);
         if(res.data!==null && res.data.IsSuccess)
         {
            this.setState({
                projectListData:res.data.ResponseResult
            });
         }             
        
        }).catch(error => {
            console.log(error);
            
            // this.setState({
            //     message:"Server error.."
            // });
          });

}
 

  formInputChange(event: any) { 

    this.setState({
      [event.target.name]: event.target.value
    });

    const validation: any = this.state.validation;
    if (!event.target.value) {
      validation[event.target.name] = 'Can\'t be empty';
    } else {
      validation[event.target.name] = '';
    }
    this.setState({
      validation
    });
  }
  
//   CreateNewNote(){
//     axios.post('http://localhost:10024/api/project/createProject', {
//     "ProjectNo":value.projectNo,
//     "ProjectName":value.projectName,
//     "ProjectLead":value.projectLead,
//     "IsDelete":false
//   }).then(res=>{
//      console.log(res.data);
//      let responseData=res.data;
//      if(!responseData.status && responseData.Message==="Project already exist")
//      {
//       alert("Project already exist");
//      }
//     else if(responseData.status && responseData.Message==="Project Created Successfully")
//     {
//       alert("Project Created Successfully");
//       window.location.reload(false);
//     }
//     else
//     {
//       alert("Some error Occured!!");
//     }     
    
//     }).catch(error => {
//         console.log(error);
//         alert("Some error Occured");
//         // this.setState({
//         //     message:"Server error.."
//         // });
//       });
//   }
  UpdateNote(){
    
  }
CheckAllValidation(event:any){
  let valid = true;
  const validation: any = this.state.validation;
   
    if(!event.target.projectNo.value)
    {   
       valid = false;        
       validation[event.target.projectNo.name]='Can not be empty';
    }     
    if(!event.target.projectName.value)
    {  
        valid = false;         
       validation[event.target.projectName.name]='Can not be empty';
    }
    if(!event.target.projectLead.value)
    {    
        valid = false;       
       validation[event.target.projectLead.name]='Can not be empty';
    }
this.setState({
  validation
});
 return valid
}

  formSubmit(event: any) {
    event.preventDefault();

    if(this.CheckAllValidation(event))
    {
  
    // if(!this.state.NoteId){
         
        axios.post('http://localhost:10024/api/project/createProject', {
            'ProjectNo':event.target.projectNo.value,
            'ProjectName':event.target.projectName.value,
            'ProjectLead':event.target.projectLead.value,
            'IsDelete':false,
          } ).then(res=>{
             console.log(res.data);
             let responseData=res.data;
             if(!responseData.status && responseData.Message==="Project already exist")
             {
              alert("Project already exist");
             }
            else if(responseData.status && responseData.Message==="Project Created Successfully")
            {
              alert("Project Created Successfully");
              window.location.reload(false);
            }
            else
            {
              alert("Some error Occured!!");
            }     
            
            }).catch(error => {
                console.log(error);
                alert("Some error Occured");
                // this.setState({
                //     message:"Server error.."
                // });
              });
    // }
    // else{
    //   this.UpdateNote()
    // }
    this.setState({ 
      title: '',
      body: '',
      noteColor:'',
      creator:''
    });
  }
  }
 
  render() {
   console.log(JSON.stringify(this.state.projectListData));
    return (
       
        <div className="col-xl-7 col-lg-7">
           
        {/* <Link to="/">Explore Project List</Link></div> */}
        {/* <Collapsible trigger="Click here to see Project list" className="Collapsible"  style={{cursor:'pointer'}}> 
        {  this.state.projectListData.map(lisdata =>{    
                              if(lisdata.ProjectNo!=null) 
                              {
                                return (
                                        <div className="col col-12" key={lisdata._id} >
                                        <ProjectList  data={lisdata}></ProjectList>
                                        </div> 
                                        )
                              }         })}     
        {this.state.projectListData.length===0?'No project found':''}     
        </Collapsible> */}
        
        <div className="card shadow mb-4">
          <div className="card-header py-3">
            <h6 className="m-0 font-weight-bold text-green">Project</h6>
          </div>
          <div className="card-body"> 
        <form onSubmit={this.formSubmit}>
          <div className="form-group">
            <label>Project Number</label>
            <input type="text" name="projectNo" placeholder="Project Number..." className="form-control" value={this.state.projectNo} onChange={this.formInputChange} />
            {this.state.validation.projectNo ? <span className="error title-error" style={{ color: 'red' }}>{this.state.validation.projectNo}</span> : null}
          </div>
          <div className="form-group">
          <label>Project Name</label>
            <input type="text" name="projectName" placeholder="Project Name..." className="form-control" value={this.state.projectName} onChange={this.formInputChange} />
            {this.state.validation.projectName ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.projectName}</span> : null}
          </div>
          <div className="form-group">
          <label>Project Lead</label>
            <input type="text" name="projectLead" placeholder="Project Lead..." className="form-control" value={this.state.projectLead} onChange={this.formInputChange} />
            {this.state.validation.projectLead ? <span className="error body-error" style={{ color: 'red' }}>{this.state.validation.projectLead}</span> : null}
          </div>   
          <div className="form-group">
            <button type="submit" className='btn btn-info' style={{marginTop:'10px'}}>{this.state.buttonName}</button>
            {/* <Link to="/Dashboard" className='float-right'>Cancel</Link> */}
          </div>
          
        </form>
        </div>
        </div>
      </div> 
    );
  }
}